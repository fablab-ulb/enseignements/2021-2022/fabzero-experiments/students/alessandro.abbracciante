# 1. Documentation

Le but du premier module  est de modifier sa page du site avec Gitlab et Markdown.

## 1.1 Git et Gitlab
J'ai deja souvent utilisé git pour d autres porojets donc cette partie a été vite. Git sert a synchroniser des projets avec un serveur, faciliter le travail en equipe en gerant la synchronisation du serveur avec les fichiers des autres developpeurs et à facilement pouvoir recuperer du code d'autres projets.

La première étape est de se connecter a son compte sur le serveur. La connexion par mot de passe est possible mais peut devenir lente quand on doit entrer son mot de passer à chaque synchronisation. Une autre méthode utilise une clé SSH. Dans ce cas, on crée d'abord un clé publique qui sera donnée au serveur et une clé privée qu'on garde secrète. Comme j'ai deja une clé SSH pour github, je vais la réutiliser en l'ajoutant sur mon compte dans la section SSH key.     

![cle SSH](../images/ssh-key.png)

La procédure que j'ai suivi pour créer les clés SSH est expliquée pour Github [ici](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/about-ssh) et reste très ressemblante pour Gitlab. 

Ensuite, je copie les fichiers du site web pour les modifier localement. 
```
git clone git@gitlab.com:fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alessandro.abbracciante.git
```
"git clone" transforme automatiquement le dossier de copie en répertoire git. Pour créer un dossier suivi par git sans cette étape, il faut commencer par : 
```
git init
```
Je peux maintenant changer ma copie des fichier. Quand je veux sauvegarder des modifications et les envoyer sur le serveur par la suite, cette commande enregistre toutes les modifications des fichiers copiés et inclut tous les nouveaux fichiers. 
```
git add -A
```

Les modifications sont envoyées au serveur uniquement dans un commit. Les commits représentent l'état du projet et servent à facilement suivre son avancée. Je pourrai aussi annuler mes modifications en cas de problème en réinitalisant le dossier à un commit antérieur. Le message de description du commit est obligatoire. 
```
git commit -m "message"
```

Finalement, pour envoyer les commits sur le serveur Gitlab : 
```
git push
``` 

## 1.2 Markdown
Les commandes Markdown que j'ai utilisé pour ce module : 
```
# pour un titre
## pour un sous titre
trois ` pour un morceau de code
![] (...) pour placer une image
[nom](url) pour un lien url 
```
[Ce site](https://www.markdownguide.org/basic-syntax/) donne une liste plus complète des commandes et leur résultat.

J'utilise visual studio code pour éditer mes modules. Un apercu des fichiers Markdown est ouvert dans un nouvel onglet en appuyant sur ctrl+maj+V.

## 1.3 Compression d'images et de vidéos

Gimp permet de rogner et compresser des images au moment de l'exportation. La taille de l'image est directement indiquée. En cochant la case "afficher l'apercu dans la fentre d'image", on peut voir si la compression n'a pas retiré trop de couleurs de l'image. [Cette page](https://www.gimp.org/tutorials/GIMP_Quickies/#changing-the-size-filesize-of-a-jpeg) contient quelques outils utiles pour rogner et tourner et reduire la taille des fichiers avec Gimp.

FFMPEG compresse les videos en ligne de commande par : 
```
ffmpeg -i nom_de_la_video -crf x nom_du_fichier_final.mp4
```
Le parametre crf effectue la compression. IL peut valoir entre 0 et 24, 24 correspond à une compression maximale. J'ai utilisé 20 pour la plupart des vidéos.

Plus de details sur la compression avec FFMPEG [ici](https://bparkerproductions.com/linux-how-to-easily-compress-a-video-with-ffmpeg/)