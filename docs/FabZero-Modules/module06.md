# 6. Electronique 2 - Fabrication

Pour la fabrication electronique, on va souder un arduino sur un circuit imprimé.

## 6.1 Composants

- un microcontrolleur [ATMEL SAMD11DC](https://www.microchip.com/en-us/product/ATSAMD11C14)
- deux résistances de 3300 ohms 
- une résitance de 110 ohms
- deux modules de 6 pins pour connecter des composants
- un régulateur de tension ZLDO 1733
- deux capacités 

## 6.2 Soudure
Le ciruit de départ est : 
![circuit imprimé](../images/rien_soude.jpg)

Pour souder un composant, la meilleure méthode est de déposer un peu d'étain sur un des connecteurs puis de poser le composant en réchauffant l'étain déposé. Ensuite, un connecteur éloigné est soudé pour bien fixer le composant pour le reste des soudures. Pour les autres soudures, on forme un lien en étalant le plus possible l'étain entre la pin du composant et sa position sur le circuit imprimé. 

Le premier composant a souder est le microcontrolleur. Un régulateur de tension qui convertit les 5V du port USB en 3.3V du microcontrolleur est ajouté ensuite. 
![uC et regulator](../images/chip_regulator.jpg)

Dès que les pins de plus sont soudés, un test est possible pour voir si le circuit fonctionne et installer un bootloader.

![pins](../images/pins.jpg)

Si tout se passe bien, le composant apparait dans la ligne de commande.
```
dmesg -c -w 
```
![dmesg](../images/dmesg.png)

J'ai soudé le reste des composants et la LED on/off. Les résistances et les capacités sont délicates a souder et il ne faut pas hésiter a recouvrir d'étain puis a les connecter au circuit. 

![led on/off](../images/led_on.jpg)

## 6.3 Debugging 
J'ai verifié les soudures au multimetre mais tout avait l'air bon. La LED s'allume bien donc le microcontrolleur est bien alimenté mais il reste un problème dans le circuit qui ne permet pas de commander la deuxième LED.