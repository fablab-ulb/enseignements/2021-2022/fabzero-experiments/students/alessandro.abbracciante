# 2. Conception Assistée par Ordinateur

Cette semaine, on a appris a utiliser deux logiciels de CAO : openscad et Freecad. La conception assistée par odinateur sert a concevoir des pieces mecaniques adaptables et de geométrie complexes par ordinateur pour les fabriquer.

## 2.1 Openscad

Openscad est un logiciel de CAO dans lequel tous les dessins sont determinés par des instructions depuis un code source. Par exemple, pour creer un cube, on utilise : 

```
cube(10,10,10); 
```

Une liste des fonctions disponibles se trouve [ici](https://openscad.org/cheatsheet/). Changer les dimensions des pieces est possible en introduisant des variables dans les fonctions et en y faisant correspondre les dimensions: 

```
l = 10; // longueur d'une arete du cube.
cube(l,l,l); 
```


Je vais maintenat produire un [flexlink](https://www.compliantmechanisms.byu.edu/flexlinks) simple avec openscad.  C'est un mecanisme qui permet de donner un degré de liberté a deux composants sans devoir incorporer de partie en mouvement. Dans ce cas, la flexion de la tige centrale permet le deplacement des attaches aux extrémités. Le résultat final est :

![resultat final](../images/fin.png)

La pièce est symétrique. Je vais la décomposer en deux morceaux en miroir qui seront unis ensuite. Chacun de ces morceaux est composé d une tige rectangulaire et d'une attache.  

![symetrie](../images/moitie.png)  

L'attache est de nouveau symétrique. Elle est composée d'un morceau rectangulaire uni avec un cylindre pour former le bord arrondi. Je retire un nouveau cylindre du tout pour percer les fixations avec d'autres Legos :

![attache](..//images/debut.png)

Le code de ce morceau est : 
```
/*
AUTHOR : Alessandro Abbracciante

LICENSE : Creative Commons Attribution-ShareAlike 3.0 Unported [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).
*/

difference(){
        //attache
        union(){
            cube([cx,cy,cz]);
     
            //morceau  arrondi exterieur
            translate([x_courbe,cy/2,0]){
                cylinder(r=r_courbe,h=cz);
            };
        };
        // percage dans la piece    
        translate([x_percage,cy/2,0]){
        cylinder(r=r_percage,h=cz);
        };
    };
```

J'ajoute maintenant la tige centrale: 

```
    translate([-long_flex/2-cx-r_courbe,cy/2,0]){
    cube([long_flex/2,cy/10,cz]);
    };
```

Le [code complet](../design_files/flex.scad) est une union de la symetrie en miroir de ce composant de base et le composant.


## 2.2 Freecad
Freecad est un logiciel de CAD graphique. Il y a un grand nombre d'outils pour arriver à une pièce. Je vais l'utiliser pour un autre flexlink : le satellite. Dans ce programme, on commence par dessiner un géometire a deux dimensions dans une esquisse puis on arrive à une géometrie en 3D par une opération de volume (souvent apr symétrie de revolution ou par extrusion). On peut finalement modifier le volume obtenu en utilisant les outils de PartDesign. 

Le satellite a une geométrie qui peut être extrudée en une seule fois. Je vais donc uniquement créer une esquisse et l'extruder. Il est aussi possible de percer la pièce apres extrusion pour former les trous de fixations des Legos mais cette operation risque de demander une nouvelle esquisse de percage. Mon esquisse finale est : 

![esquisse](../images/esquisse.png)

J'ai créée cette esquisse par symetrie en dessinant a partir de la moitié droite. L'esquisse de déoart n'est pas contrainte. Il faut rajouter des valeurs a certaines dimensions comme les longeurs des segment ou les angles jusqu'à contraindre complètement l'esquisse. Pour garder la possibilité de modifer les valeurs des contraintes, je vais les definir dans une cellule d'un tableur. Je nomme ensuite la cellule selon l'option alias du tableur. Le nom de la cellule peut alors etre introduit dans la contrainte.

Le haut du satellite est en trois morceaux: un rectangle horizontal, un trapèze et un demi-cercle qui vient fermer le dessin. Les contraintes de cette partie sont des coincidentes pour que les points d'intersection des morceaux ne se séparent pas et deux contraintes horizontales pour les lignes horizontales du rectangle. Pour entièrement contraindre le haut, je précise plusieurs dimensions : 

![contraintes de dimensions](../images/haut.png)

Le bas est plus difficile a construire. La fine tige est aussi un rectangle mais le bord du bas ne peut pas couper le cercle de l'attache basse sinon l'extrusion echouera. J'ai contraint ce point le liant à l'arc de cercle par une coincidente. Le reste est composé de lignes en contrainte verticale et du demi-cercle qui ferme le morceau.

![contraintes de dimensions bas](../images/bas.png)

J'ai eu plusieurs problèmes pour cette pièce :  la symétrie n'a pas copié les contraintes donc j'ai du les mettre à la main. L'ordre des changements de dimensions est important. En réduisant la taille de certaines tiges, le modèle ne s'est pas reconstruit. Il faut changer toutes les dimensions puis faire un clic droit sur l'esquisse et recalculer l'objet pour éviter ce risque.  

Les fichiers sont disponibles dans le dossier [design_files](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alessandro.abbracciante/-/tree/main/docs/design_files)  sous licence CC BY-SA 3.0 (Creative Commons Attribution-ShareAlike 3.0 Unported [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)). Le satellite est le fichier satellite.FCStd et le flexlink simple flexlink_simple.scad.