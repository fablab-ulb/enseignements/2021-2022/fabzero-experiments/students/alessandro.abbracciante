# 4. Impression 3D

## 4.1 Prusa 
Les imprimantes du fablab sont des prusa MK3 et prusa MK3S. Le lit de l'imprimante est chauffé pour améliorer l'adhérence du filament. Le plateau se deplace dans une direction et la buse se déplace verticalement et dans l'autre direction.

Les deux filaments disponibles sont le PLA moins résistants mais économique et tres facile à recycler et le PETG qui est légerement toxique mais plus résistant.

## 4.2 Paramètres d'impression
Les imprimantes 3D ne lisent que des fichiers Gcode. Ce sont des fichierd qui donnent des insrtuctions pour déplacer la buse de l'imprimante  selon la géometrie. Le logiciel prusa slicer permet de générer un gcode a partir d'un fichier STL.

La hauteur de couche est la hauteur de filament que l'on ajoute à chaque passe. L'idéal est de choisir la moitié du diamètre de la buse (0.2mm) mais des hauteurs un peu plus fines (0.1mm environ) pour une meilleure résistance ou plus grande (0.3mm) pour une impression plus courte ne posent pas de problèmes.

L'épaisseur des couches solides est le nombre de couches pleines aux extrémites verticales et horizontales de la pièce. L'intérieur sera creux et rempli suivant un schéma particulier.

![schéma de remplissage](../images/motifs.jpg)

Le remplissage peut varier entre 5% et 35%. Plus le remplissage est important, plus la résistance aux contraintes mécaniques est améliorée et l'impression est longue. Au dela de 35%, les propriétés mécaniques n'augmentent plus et le plastique supplémentaire est inutile. Les schémas de remplissages peuvent rendre la pièce plus souple ou rigide. Le schéma gyroide ou honeycomb offrent une bonne solidité.

La jupe est le contour de la zone à imprimer. La bordure est la largeur supplémentaire des premières couches qui servent à augmenter la rigidité de la base pour éviter que les hautes structures ne plient au cours de l'impression.

L'ensembles des réglages habituels pour la Prusa est [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/master/vade-mecum/3D_print.md)


## 4.3 Torture test
Le torture test sert à analyser les situations limite pour l'imprimante 3D. Pour la Prusa, un agle de plus de 30° pour la pertie inclinée mène à des couches mal formées et qui coulent légerement au niveau du bas. Un pièce qui présente ce type d'angle devra etre construire en ajoutant des supports. Les petites tiges verticales du torture test sont très fragiles et parfois mal fixée à la plaque. Pour ce genrede tige, des couches de bordure supplémentaires sont nécessaires.

![torture test](../images/torture_test.jpg)

## 4.4 Flexlinks
[design_files](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alessandro.abbracciante/-/tree/main/docs/design_files)  sous licence CC BY-SA 3.0 (Creative Commons Attribution-ShareAlike 3.0 Unported [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/))

Pour garder une bonne flexibilité des flexlinks, je vais imprimer la tige en 1mm de largeur et 10mm de longeur. Ce ne sont pas des géometries pour lequelles un support ou des changements de valeurs de bordure doivent etre appliqués. 

Le premier flexlink simple plie très bien : 

<video controls muted>
<source src=../../images/flex.mp4>
</video>

[(lien alternatif)](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alessandro.abbracciante/-/blob/main/docs/images/flex.mp4)

Le satellite est plus rigide mais garde une mobilité de la partie centrale : 
<video controls muted>
<source src=../../images/satellite2.mp4>
</video>

[(lien alternatif)](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alessandro.abbracciante/-/blob/main/docs/images/satellite2.mp4)