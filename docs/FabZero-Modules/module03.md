# 3. Découpe assistée par ordinateur

La découpe au laser permet de graver ou decouper précisément de grande surfaces. Avant de commencer une découpe, il faut vérifier quelques précautions d'usage et que le matériau utilisé est sans danger. 

## 3.1 Matériaux et précautions d'usages
Il y a trois découpeuses différentes au fablab. La lasersaur est la plus puissante, on ne peut pas s'en éloigner pour éviter tout risque d'incendie. Quand on scanne  le QR code pour valider sa présence, la machine demande régulièrement de confirmer sa présence en appyant sur le bouton 'Je suis ici'. Si cette étape n'est pas validée, la lasersaur s'éteindra par sécurité. 

Toutes les autres précautions sont valables pour toutes les découpeuses : 
- Des extracteurs de fumée évacuent la fuméee produite lors de la découpe et doivent toujours être activés avant de lancer l'opération. 
- le circuit de refroidissement à air comprimé doit aussi être actif. Si on tente de découper sans air comprimé, la découpeuse ne démarrera pas.

La chaleur du laser peut provoquer des etincelles ou un debut de feu. Une couverture et un extincteur se trouvent dans la pièce près de la porte en cas de besoin.

Certains plastiques dégagent des fumées toxiques et sont interdits. Les planches en bois fines peuvent être utilisées sans problèmes.

La liste complète des matériaux a utiliser ou non se trouve [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md)

## 3.2 Caractérisation de la lasersaur
Les instructions d'utilisation de la lasersaur sont dans [ce document](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md). Les éléments à vérifier sont : 

Le but de cette étape est de caractériser une découpeuse laser. La calibration se fait en variant progressivement la puissance et la vitesse de la lasersaur pour voir quelles sont les cases qui correspondent à une découpe et celles qui correspondent à une gravure ainsi que la profondeur de la gravure.

![calibration](../images/calibration_image.jpg)

Les résultats de la calibration de la lasersaur montrent qu'a partir de 40W, les bois est coupé a toutes les vitesses. Les vitesses les plus rapides sous cette puissance gravent seulement, la profondeur de la gravure augmente avec la puissance.

## 3.3 Kirigami

Un kirigami est une forme à deux ou trois dimensions que l'on crée uniquement en découpant et pliant une feuille de papier. Je vais faire un pot en carton de cette manière. 

Pour plier plus facilement le carton, une gravure sera utilisée pour marquer et préparer les pliages. Les découpeuses laser peuvent accepter des paramètres différents selon la couleur du dessin. Je vais utiliser inkscape pour tracer deux formes : le contour extérieur à couper et les lignes à marquer. Le patron utilisé pour ce kirigami est le suivant:

![patron](../images/kiri1.jpg)

[(source de l'image)](https://www.pinterest.ch/pin/302444931222781030/) 

J'importe l'image dans Inkscape et je trace le bord extérieur avec l'outil pour tracer des segments de droite (touche B). 
![contour](../images/contour.png)

Je dessine en rouge les traits interieurs. Les courbes et l'image ne sont pas liés et on peut supprimer l'image ensuite.
![tout](../images/tout.png)

Pour que la découpeuse laser puisse lire le fichier, il doit etre exporté en SVG. Maintenant, je dois choisir les réglages de vitesse pour graver et découper. Le carton que j'utiliserai est deja calibré donc les parametres peuvent etre recupérés. Pour découper, la puissance et de 80% de la puissance maximale et la vitesse de 50% de la vitesse maximale. Pour graver, j'utilise une puissance de 70% et une vitesse de 15%.

Le resultat de la découpe : 

![decoupe](../images/decoupe.jpg)

Le pot assemblé : 

![pot complet](../images/pot.jpg)

Les fichiers sont disponibles dans le dossier [design_files](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alessandro.abbracciante/-/tree/main/docs/design_files)  sous licence CC BY-SA 3.0 (Creative Commons Attribution-ShareAlike 3.0 Unported [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)). Le kirigami est le fichier kirigami.svg .