# 5. Electronique 1 - Prototypage

La première partie du module électronique concerne le prototypage a l'aide d'un arduino.

## 5.1 Précautions
L'arduino va etre utilisé en l'alimentant par un port USB. Pour éviter d'endommager le port USB et l'arduino, il ne faut pas provoquer de court-ciruit. Tout les branchements de composants  à l'arduino doivent donc se faire sans alimentation par l'ordinateur pour qu'un contact du 5V et de la terre en rebranchant des fils ne crée pas de court circuit. 

Un autre danger est de demander un courant excessif à l'arduino. Toutes les pins de l'arduino peuvent etre utilisées en input ou en ouput mais le risque se présente surtout lors d'une connexion en input. Les pins connectées en output sont à haute impéndance et le courant y est plus faible. Une pin connectée en tant qu'input ne peut pas fournir un courant de plus de 40mA maximum et 20mA sont recommandés. Sur l'image des différents pins ci dessous, des cercles jaunes et rouges sont représentés. Ces groupes de pins ne résistent pas à un courant de lus de  100 mA au total. Il faut donc aussi vérifier le courant admis dans chaque groupe. Le courant de l'ensemble de l'arduino est aussi limité à 200mA. 

![pinout](../images/Arduino-uno-pinout.png)

## 5.2 Utilisation des I/O
Il y a deux types d'entrées/sorties sur l'arduino. Les entrées numériques lisent des signaux pouvant prendre soit une valeur proche de 5V soit proche de 0V. Les entrées analogiques lisent des signaux pouvant prendre n'importe quelle valeur entre 0 et 5V.

Les sorties numériques produisent un signal qui peut aussi prendre soit une valeur proche de 5V soit une valeur proche de 0V. Les sorties analogiques utlisent de la modulation de largeur d'impulsion (MLI/PWM).
![PWM](../images/PWM.jpeg)
La MLI ne génère pas de véritables signaux analogiques mais des signaux de 0 ou 5V variant à haute fréquence et dont la composante fondamentale vaut le signal analogique voulu. Le signal carré généré introduit des harmoniques de haute fréquence distordant le signal. Les sorties analogiques ne sont pas utilisables si la sortie ne filtre pas les hautes frequences et que la distorsion doit etre limitée.

Pour les applications ou la puissance est supérieure au courant maximum de l'arduino, une alimentation externe doit etre ajoutée.


## 5.3 Code
Pour programmer l'arduino, différentes librairies en C sont disponibles depuis l'IDE. Un grand nombre de fonctionnalités de base sont implémentées et peuvent etre réutilisées en les important via 
```C
#include <library.h>
```
La partie principale du programme est composée de deux fonctions : setup() et loop(). La fonction
setup permet de définir quelles sont les pin utilisées et leur fonction. Les pins d'entrées analogiques servent uniquement en tant qu'entrée mais les autres pins peuvent etre declarées en input ou en output.

La fonction loop est une boucle qui sert à lire et ecrire en temps réel les valeurs des pins.


## 5.4 Exemple simple
L'exemple le plus simple est de faire varier l'intensité d'une LED au cours du temps. Le code de cet exemple peut etre trouvé directement dans les exemples basiques d'arudino. Le branchement consiste à placer une résistance en série avec la LED et d'alimenter le tout en 5V. 
![LED](../images/led.jpg)
![](../images/fade.mp4)

Le code peut etre trouvé sur le site [arduino](https://docs.arduino.cc/built-in-examples/basics/Fade).

```
/*
  Fade

  This example shows how to fade an LED on pin 9 using the analogWrite()
  function.

  The analogWrite() function uses PWM, so if you want to change the pin you're
  using, be sure to use another PWM capable pin. On most Arduino, the PWM pins
  are identified with a "~" sign, like ~3, ~5, ~6, ~9, ~10 and ~11.

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Fade
*/

int led = 9;           // the PWM pin the LED is attached to
int brightness = 0;    // how bright the LED is
int fadeAmount = 5;    // how many points to fade the LED by

// the setup routine runs once when you press reset:
void setup() {
  // declare pin 9 to be an output:
  pinMode(led, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  // set the brightness of pin 9:
  analogWrite(led, brightness);

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
  if (brightness <= 0 || brightness >= 255) {
    fadeAmount = -fadeAmount;
  }
  // wait for 30 milliseconds to see the dimming effect
  delay(30);
}
```

## 5.4 Utilisation d'un capteur
La liste des capteurs disponible est [ici](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero/electronics/-/blob/07e9c48531529f84506c966be76957e4dba6ec1c/IO-Modules.md)
Je vais utiliser une photorésistance. Pour pouvoir mesurer la variation de la resitance avec la lumière, un diviseur résistif est nécéssaire. Une resistance est mise en série avec la photorésitance et le tout est alimenté en 5V. La mesure se fait en parallèle de la photorésistance.

La correspondance entre la résistance mesurée au niveau de la photorésistance et l'intensite de la lumière en lux est donée par [cette datasheet](https://www.arduino.cc/documents/datasheets/LDR-VT90N2.pdf) 

## 5.5 Difference de luminosité par LED
En reprenant les exemples plus haut, je vais faire varier l'intensité d'une LED selon la différence de luminosité entre deux photorésistances. Les deux photorésitances sont en parallèles et le signal traité par l'arduino est envoyé vers la led.

```C
/*
AUTHOR : Alessandro Abbracciante

LICENSE : Creative Commons Attribution-ShareAlike 3.0 Unported [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).
*/
int led_out = 5;
int brightness = 0;    // how bright the LED is
int sensor1 = A0;    // first light sensor 
int sensor2 = A1;    // light sensor 2 
int sensor1Value = 0;  // variable to store the value coming from the sensor 1
int sensor2Value = 0;  // variable to store the value coming from the sensor 2


void setup() {
    pinMode(led_out, OUTPUT); // output LED
    Serial.begin(9600); // to start a connection by the USB port and plot light intensity
}

void loop() {
    sensor1Value = analogRead(sensor1); // read the first resistor
    sensor2Value = analogRead(sensor2); // read the other resistor 
    brightness = abs(sensor1Value - sensor2Value); // difference in brightness 
    analogWrite(led_out, brightness); // writing the value to the LED
    Serial.println(brightness); // prints the data to the computer USB port
}
```
La mesure de la différence oscille avec la différence de luminosité ambiante et augmente fortement lorsqu'une des photoresitances est couverte.
![luminosité](../images/led_brightness.PNG)

Le montage est composé de deux montages pour capteur en parallèle et d'une led connetéee comme précédemment.
![montage complet](../images/delta.jpg)

<video controls muted>
<source src=../../images/delta.mp4>
</video>

[(lien alternatif)](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/alessandro.abbracciante/-/blob/main/docs/images/satellite2.mp4)