Bonjour!

Je suis Alessandro !!


## A propos de moi
Bonjour, je suis Alessandro Abbracciante. Je suis en MA2 ingénieur électromécanique en option robotique. 

## Autres projets
J'ai surtout fait des projets pour mes cours. En BA2, j'ai fait un projet de robot qui dessine sur un tableau. En MA1, mon projet d'année était d'installer une caméra sur un drone pour mesurer son altitude grace à des marqueurs au sol de dimensions connues. J'ai aussi construit un robot qui remplit du papier bulle pour un projet de groupe au fablab de la VUB.  

