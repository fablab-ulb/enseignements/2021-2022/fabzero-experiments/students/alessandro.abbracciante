# Final Project

Je fais le projet final en commun avec Sajl Ghani. Sa page de projet final se trouve sur [ce lien.
](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/sajl.ghani/final-project/)


## Engage - Find a challenge that motivates you

### Big ideas - What is the broad theme or concept I would like to explore 

Le theme que je voudrais explorer est la mobilité douce. Dans les grandes villes, la reduction des émissions de CO2 impose de transformer nos moyens de déplacement vers des solutions écologiques. Le vélo est une solution fréquemment reccommandée pour les trajets courts en milieu urbain. Les trotinettes électriques sont une autre solution pour ces petits déplacements. Pourtant, leur usage est assez réduit et pourrait croitre largement.  

![comparaison emissions velo vs voitures](images/velovscar.png)
[source de l'image](https://www.liberation.fr/environnement/entre-le-velo-et-la-voiture-une-difference-stratospherique-demissions-de-co2-20211008_WAZ5WETJ7RGFVPLAKGNTCGCNXQ/)

### Essential questions - What are the essential questions that reflect personal interests and the needs of the community ? 
-Quels sont les obstacles au déploiement du vélo ?

-Comment les outils du fablab peuvent intervenir sur ce sujet ?

### Challenge - what is your call for action ?
-Comment obtenir un produit abordable avec les contraintes du fablab dans le cadre de la mobilité ?


## Investigate - step on the shoulders of the giants not on their toes
### Guiding questions - what are all the questions that needs to be answered in order to tackle the challenge ? Priority ?
-Quelles techniques du fablab employer ? 

-Combien coutent les composants ?

-Quelles solution existent déja ? 

-Que peut on apporter en respectant les contraintes et le budget ?


### Analysis and synthesis - write a summary of your findings, facts and data collected
Augmenter le vélo pour les court trajets est un élément majeur pour la réduction des émission de gaz à effet de serre en ville. Les émission du secteur des transports sont parmi les premières dans les pays développés [(source)](https://www.fub.fr/velo-ville/environnement/velo-atout-environnement). Agir dans ce domaine peut remplir l'objectif 11 des objectifs de déveloopement durable de l'ONU : "Make cities and human settlements inclusive, safe, resilient and sustainable." en améliorant la sécurité et la qulité de vie au sein des centre urbains. L'objectif 13 : "Take urgent action to combat climate change and its impacts" est également abordé par l'aspect écologique du transport.   

Selon la fédération des usagers de la bicyclette, la sécurité est un élément important des réticenses à l'utilisation du vélo [(source)](https://www.flotauto.com/fub-insecurite-ralentit-adoption-velo-20171208.html). Les automobilistes ont aussi un lien avec le manque de sécurité du vélo. 

En analysant des solutions existantes, un casque de feu arrière de vélo nous a paru pouvoir être amélioré. Le casque  de cycliste permet de garantir la sécurité du cycliste à la fois en se protégeant la tête et en signalant aux autres usagers. Un système de clignotants intégré indique la direction que va prendre le cycliste et ajoute donc à la sécurité en communicant son itinéraire. Les solutions existantes, comme le cosmo dont le prix est de 75 €. Un prix plus bas encouragerait a l'ajouter à un casque. 


![casque cosmo](images/helmet.PNG) 


## Act - Develop a solution, implement it and get feedback
### premier prototype
La solution qu'on propose consiste à utiliser un fablab pour diminuer le prix d'un système de clignotants pour casque. 


L'inclinaison de la tête est mesurée en utilisant un capteur MPU 9150. Ce capteur inclut un gyroscope et des accéléromètres à trois axes, un magnétomètre et un capteur de température. Les données de ces capteurs sont fusionnées par un microcontrolleur pour affiner la précision des mesures des angles d'inclinaison. Le capteur communique ces données selon le protocole I2C. [(datasheeet du MPU 9150)](https://invensense.tdk.com/wp-content/uploads/2015/02/MPU-9150-Datasheet.pdf)

La connexion par I2C demande de donner une adresse au capteur. L'adresse configurée par le constructeur commence par 110100X. Le dernier bit n'est pas fixée pour pouvoir connecter deux capteurs en série. L'entrée AD0 du capteur indique la valeur choisie. Par la suite, ce bit sera toujours mis à 0.

Le protocole I2C fonctionne en configuration maitre-esclave. L'arduino sera le maitre et va demander régulièrement l'inclinaison des capteurs. 

![I2C connection](images/i2c.png)  
[source de l'image](https://www.circuitbasics.com/basics-of-the-i2c-communication-protocol/)

Les ports SDA et SCL de l'arduino se connectent aux ports identiques du capteur. Un premier test peut alors etre lancé pour mesurer l'inclinaison de la tête du cycliste. Le code qui suit est le code utilisé pour ce test simple.  

```C
// minimal MPU-6050 tilt and roll (sjr)
// works perfectly with GY-521, pitch and roll signs agree with arrows on sensor module 7/2019
// tested with eBay Pro Mini, **no external pullups on SDA and SCL** (works with internal pullups!)
//
#include<Wire.h>
const int MPU_addr1 = 0x68;
float xa, ya, za, roll, pitch;

void setup() {

  Wire.begin();                                      //begin the wire communication
  Wire.beginTransmission(MPU_addr1);                 //begin, send the slave adress (in this case 68)
  Wire.write(0x6B);                                  //make the reset (place a 0 into the 6B register)
  Wire.write(0);
  Wire.endTransmission(true);                        //end the transmission
  Serial.begin(9600);
}

void loop() {

  Wire.beginTransmission(MPU_addr1);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr1, 6, true); //get six bytes accelerometer data

  xa = Wire.read() << 8 | Wire.read();
  ya = Wire.read() << 8 | Wire.read();
  za = Wire.read() << 8 | Wire.read();

  roll = atan2(ya , za) * 180.0 / PI;
  pitch = atan2(-xa , sqrt(ya * ya + za * za)) * 180.0 / PI;

  Serial.print("roll = ");
  Serial.print(roll,1);
  Serial.print(", pitch = ");
  Serial.println(pitch,1);
  delay(400);
}
```

Quand un basculement de la tête est détecté, une série de LED clignote du coté correspondant. Pour cette première version du code, un changement d'angle démarre une boucle de clignotement des LED durant 10 secondes.

```C
/*
    FILE    : proto_code.ino

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com> and Alessandro Abbracciante <alessandro.abbracciante@ulb.be>

    DATE    : 2022-05-12

    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

    Based on : minimal MPU-6050 tilt and roll (sjr) from https://forum.arduino.cc/t/mpu6050-unstable-values-drift/624303/2

    and NeoPixel Ring simple sketch from  Adafruit NeoPixel library

*/


#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#include<Wire.h>
const int MPU_addr1 = 0x68;
float xa, ya, za, roll, pitch, pre_pitch;
unsigned long start;
float limit_right;
float limit_left;

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 3 // Popular NeoPixel ring size

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN_red        6 // On Trinket or Gemma, suggest changing this to 1
#define PIN_green        5 // On Trinket or Gemma, suggest changing this to 1

// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN_red, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMPIXELS, PIN_green, NEO_GRB + NEO_KHZ800);

#define DELAYVAL 500 // Time (in milliseconds) to pause between pixels


void setup() {
  // put your setup code here, to run once:

  Wire.begin();                                      //begin the wire communication
  Wire.beginTransmission(MPU_addr1);                 //begin, send the slave adress (in this case 68)
  Wire.write(0x6B);                                  //make the reset (place a 0 into the 6B register)
  Wire.write(0);
  Wire.endTransmission(true);                        //end the transmission
  Serial.begin(9600);

  pre_pitch=0.0;

  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  // END of Trinket-specific code.

  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)

}

void loop() {
  // put your main code here, to run repeatedly:
  pixels.setPixelColor(0, pixels.Color(0, 0, 0));
  pixels.setPixelColor(1, pixels.Color(0, 0, 0));
  pixels.setPixelColor(2, pixels.Color(0, 0, 0));
  pixels.show();   // Send the updated pixel colors to the hardware.

  strip.setPixelColor(0, pixels.Color(0, 0, 0));
  strip.setPixelColor(1, pixels.Color(0, 0, 0));
  strip.setPixelColor(2, pixels.Color(0, 0, 0));
  strip.show();   // Send the updated pixel colors to the hardware.

  Wire.beginTransmission(MPU_addr1);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr1, 6, true); //get six bytes accelerometer data

  xa = Wire.read() << 8 | Wire.read();
  ya = Wire.read() << 8 | Wire.read();
  za = Wire.read() << 8 | Wire.read();

  roll = atan2(ya , za) * 180.0 / PI;
  pitch = atan2(-xa , sqrt(ya * ya + za * za)) * 180.0 / PI;

  Serial.print("roll = ");
  Serial.print(roll,1);
  Serial.print(", pitch = ");
  Serial.println(pitch,1);

  limit_right=40;

  stop_clignotant=10000; // 10 second

  if (pitch-pre_pitch >limit_right){
      start = millis();
      while (millis() - start <stop_clignotant){
        Serial.println(millis() - start);
        pixels.setPixelColor(0, pixels.Color(250, 0, 0));
        //pixels.show();   // Send the updated pixel colors to the hardware.
        pixels.setPixelColor(1, pixels.Color(250, 0, 0));
        //pixels.show();   // Send the updated pixel colors to the hardware.
        pixels.setPixelColor(2, pixels.Color(250, 0, 0));
        pixels.show();   // Send the updated pixel colors to the hardware.

        delay(DELAYVAL); // Pause before next pass through loop
        pixels.setPixelColor(0, pixels.Color(0, 0, 0));
        pixels.setPixelColor(1, pixels.Color(0, 0, 0));
        pixels.setPixelColor(2, pixels.Color(0, 0, 0));
        pixels.show();   // Send the updated pixel colors to the hardware.
        delay(DELAYVAL); // Pause before next pass through loop
      }
  }

  limit_left=40;

  if (pitch-pre_pitch <-limit_left){
      start = millis();
      while (millis() - start <stop_clignotant){
        Serial.println(millis() - start);
        strip.setPixelColor(0, pixels.Color(0, 250, 0));
        strip.setPixelColor(1, pixels.Color(0, 250, 0));
        strip.setPixelColor(2, pixels.Color(0, 250, 0));
        strip.show();   // Send the updated pixel colors to the hardware.

        delay(DELAYVAL); // Pause before next pass through loop
        strip.setPixelColor(0, pixels.Color(0, 0, 0));
        strip.setPixelColor(1, pixels.Color(0, 0, 0));
        strip.setPixelColor(2, pixels.Color(0, 0, 0));
        strip.show();   // Send the updated pixel colors to the hardware.
        delay(DELAYVAL); // Pause before next pass through loop
      }
  }

  delay(400);

}
```

### deuxième prototype
Cette première version du code pose un problème. Les LED clignotent pendant 10 secondes peut importe ce qu'il se passe ensuite. Nous voudrions ajouter la possibilité de changer le clignotement des LED si l'utilisateur se penche de l'autre coté, en cas d'erreur ou de tournants plus courts.  La progrmammation est modifiée pour qu'à chaque cycle de 400ms, on vérifie si l'inclinaison de la tête ne dépasse pas la limite de l'autre coté. Si c'est le cas, le clignotement est interrompu sur une série de LED et démarre du bon coté. Chaque cycle diminue un compteur qui arrete le clignotement quand les 10 secondes se sont écoulées. 


```C
/*
    FILE    : proto_code.ino

    AUTHOR  : Ghani Sajl <sajl99@hotmail.com> and Alessandro Abbracciante <alessandro.abbracciante@ulb.be>

    DATE    : 2022-05-12

    license : Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) (https://creativecommons.org/licenses/by-nc/4.0/)

    Based on : minimal MPU-6050 tilt and roll (sjr) from https://forum.arduino.cc/t/mpu6050-unstable-values-drift/624303/2

    and NeoPixel Ring simple sketch from  Adafruit NeoPixel library

*/

#include <Adafruit_NeoPixel.h>
#include <Wire.h>

#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif


// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 3 // Popular NeoPixel ring size

// Which pin on the Arduino is connected to the NeoPixels?
#define PIN_red        6 // On Trinket or Gemma, suggest changing this to 1
#define PIN_green        5 // On Trinket or Gemma, suggest changing this to 1

#define DELAYVAL 500 // Time (in milliseconds) to pause between pixels
#define BLINK_TIME 10000 // blink time in ms 


// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN_red, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMPIXELS, PIN_green, NEO_GRB + NEO_KHZ800);


const int MPU_addr1 = 0x68;
float xa, ya, za, roll, pitch, pre_pitch;
unsigned long start_time;
float limit_right;
float limit_left;
bool tilt_left = false;
bool tilt_right = false;
int counter = BLINK_TIME; 


void setup() {
  // put your setup code here, to run once:

  Wire.begin();                                      //begin the wire communication
  Wire.beginTransmission(MPU_addr1);                 //begin, send the slave adress (in this case 68)
  Wire.write(0x6B);                                  //make the reset (place a 0 into the 6B register)
  Wire.write(0);
  Wire.endTransmission(true);                        //end the transmission
  Serial.begin(9600);

  pre_pitch=0.0;
  
  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
  #endif
  // END of Trinket-specific code.

  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  
  pixels.clear();
  pixels.show();   // Send the updated pixel colors to the hardware.

  strip.clear();
  strip.show();   // Send the updated pixel colors to the hardware.

}

void loop() {
  // put your main code here, to run repeatedly:

  start_time = millis(); // start couting the loop duration
 
  Wire.beginTransmission(MPU_addr1);
  Wire.write(0x3B);
  Wire.endTransmission(false);

  Wire.requestFrom(MPU_addr1, 6, true); //get six bytes accelerometer data
  xa = Wire.read() << 8 | Wire.read();
  ya = Wire.read() << 8 | Wire.read();
  za = Wire.read() << 8 | Wire.read();

  roll = atan2(ya , za) * 180.0 / PI;
  pitch = atan2(-xa , sqrt(ya * ya + za * za)) * 180.0 / PI;

  Serial.print("roll = ");
  Serial.print(roll,1);
  Serial.print(", pitch = ");
  Serial.println(pitch,1);

  limit_right=40;
  limit_left=40;
  
  if (pitch-pre_pitch >limit_right){
      tilt_right = true;
      tilt_left = false; // stop left led if it was on
      counter = BLINK_TIME; // reset counter if other led was on

  }


  if (pitch-pre_pitch <-limit_left){
      tilt_left = true;
      tilt_right = false; // stop right led if it was on
      counter = BLINK_TIME; // reset counter if other led was on

  }

  if(tilt_left){
    strip.setPixelColor(0, pixels.Color(250,0,0));
    strip.setPixelColor(1, pixels.Color(250,0,0)); 
    strip.setPixelColor(2, pixels.Color(250,0,0)); 
    strip.show();   // Send the updated pixel colors to the hardware.
    delay(DELAYVAL); // Pause before next pass through loop
    strip.clear();
    strip.show();   // Send the updated pixel colors to the hardware.
    delay(DELAYVAL); // Pause before next pass through loop
    counter -= 400 + millis() - start_time;
  }

  if(tilt_right){
    pixels.setPixelColor(0, pixels.Color(0,250,0));
    pixels.setPixelColor(1, pixels.Color(0,250,0)); 
    pixels.setPixelColor(2, pixels.Color(0,250,0)); 
    pixels.show();   // Send the updated pixel colors to the hardware.
    delay(DELAYVAL); // Pause before next pass through loop
    pixels.clear();
    pixels.show();   // Send the updated pixel colors to the hardware.
    delay(DELAYVAL); // Pause before next pass through loop
    counter -= 400 + millis() - start_time;
  }

  if (counter <= 0) {
    counter = BLINK_TIME;
    tilt_left = false;
    tilt_right = false;
  }


  delay(400);

}
```
