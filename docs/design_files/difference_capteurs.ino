/*
AUTHOR : Alessandro Abbracciante

LICENSE : Creative Commons Attribution-ShareAlike 3.0 Unported [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).
*/
int led_out = 5;
int brightness = 0;    // how bright the LED is
int sensor1 = A0;    // first light sensor 
int sensor2 = A1;    // light sensor 2 
int sensor1Value = 0;  // variable to store the value coming from the sensor 1
int sensor2Value = 0;  // variable to store the value coming from the sensor 2


void setup() {
  // output led:
    pinMode(led_out, OUTPUT);
    Serial.begin(9600);
}

void loop() {
    sensor1Value = analogRead(sensor1);
    sensor2Value = analogRead(sensor2);
    brightness = abs(sensor1Value - sensor2Value); 
    analogWrite(led_out, brightness);
    Serial.println(brightness);
}
