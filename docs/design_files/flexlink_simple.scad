/*
AUTHOR : Alessandro Abbracciante

LICENSE : Creative Commons Attribution-ShareAlike 3.0 Unported [CC BY-NC-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).
*/

$fn=500;
//2*longueur de la partie cubique de l attache
cx = 4;
//largeur de l attache
cy = 10;
//hauteur de l attache
cz = 3;
//dim percage
r_percage = 2.4;
x_percage = cx;
//dim courbure exterieure
r_courbe = cy/2;
x_courbe = cx;
//2*dim barre flexible centrale
long_flex = 20;

union(){
mirror([1,0,0]){
translate([long_flex/2+cx+r_courbe,0,0]){
union(){
    
    difference(){
        //attache
        union(){
            cube([cx,cy,cz]);
     
            //morceau  arrondi exterieur
            translate([x_courbe,cy/2,0]){
                cylinder(r=r_courbe,h=cz);
            };
        };
        // percage dans la piece    
        translate([x_percage,cy/2,0]){
        cylinder(r=r_percage,h=cz);
        };
    };
    
    //miroir
    mirror([1,0,0]){
    
    difference(){
        //attache
        union(){
            cube([cx,cy,cz]);
     
            //morceau  arrondi exterieur
            translate([x_courbe,cy/2,0]){
                cylinder(r=r_courbe,h=cz);
            };
        };
        // percage dans la piece    
        translate([x_percage,cy/2,0]){
        cylinder(r=r_percage,h=cz);
        };
   };
   };

    translate([-long_flex/2-cx-r_courbe,cy/2,0]){
    cube([long_flex/2,cy/10,cz]);
    };
};
};
};

translate([long_flex/2+cx+r_courbe,0,0]){
union(){
    
difference(){
    union(){
        cube([cx,cy,cz]);
 
        //morceau  arrondi exterieur
        translate([x_courbe,cy/2,0]){
            cylinder(r=r_courbe,h=cz);
        };
    };
// percage dans la piece    
translate([x_percage,cy/2,0]){
cylinder(r=r_percage,h=cz);
};
};
    
mirror([1,0,0]){
difference(){
    union(){
        cube([cx,cy,cz]);
 
        //morceau  arrondi exterieur
        translate([x_courbe,cy/2,0]){
            cylinder(r=r_courbe,h=cz);
        };
    };
// percage dans la piece    
translate([x_percage,cy/2,0]){
cylinder(r=r_percage,h=cz);
};
};
};

translate([-long_flex/2-cx-r_courbe,cy/2,0]){
cube([long_flex/2,cy/10,cz]);
};
};
};
};


